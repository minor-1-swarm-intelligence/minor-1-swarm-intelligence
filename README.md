# Minor Project I
### An efficient approach to find the optimal network route using Swarm Intelligence.

### Project SDLC Model => Iterative Enhancement Model

We use Iterative enhancement model by creating a product backlog and dividing the whole project into sub modules which will be developed by all members in a distributed development environment using Git VCS.

# Members:

 [Harsh Joshi](https://www.github.com/josharsh)
 [Priyanka Yadav](https://www.github.com/Priyanka488)
 [Nishkarsh Raj](https://www.github.com/NishkarshRaj)
 [Lakshika Parihar](https://www.github.com/lakshika1064)
 
# Abstract

In today’s world, fast and efficient communication on network between the sender and receiver is very important. For this communication, data is converted into packets and sent over the network using routing algorithms. In a network or over multiple networks, routing refers to the process of determining a path for a packet to travel from. The traditional algorithms used in networking for finding the minimum spanning tree and shortest path include Prim’s algorithm and Kruskal’s Algorithm. These methods solve the problem of traffic in networking in the narrow investigation of search space and hence result in inferior solutions. In this project, we aim to propose the basic idea of optimizing network routing using swarm intelligence. Particle swarm intelligence is a technique that utilizes the behavior of self-organizing, decentralized systems. It is considered to be a very optimum global search algorithm.

*Keywords*: _Network Routing, Optimization, Swarm Intelligence, PSO Algorithm, Minimum Spanning Tree_

# Aim
This project aims to compare and summarize the computer network routing strategies while investigating the optimization of these using *PSO technique*. The project focuses on minimal spanning tree and solves the shortest path problem using swarm intelligence. Particle Swarm Intelligence utilizes the combination of the behavior of swarms which is considered efficient for optimization and discrete multidimensional problems. 

# Particle Swarm Optimization
In Particle Swarm Optimization Algorithm, a subset of Swarm Intelligence, we optimize network routing by modifying traditional graph algorithms like Minimum Spanning trees by Kruskal’s or Prim’s methods by using cognitive intelligence algorithm to get better optimization and solve MDR(Multi Destination Routing) problem which cannot be solved effectively using traditional methods.

Particle Swarm Optimization algorithm implementation on network routing gives fast convergence speed, has an easy implementation, is cognitive and supports dynamic traffic.


# How to Collaborate:

1. Fork the repository to your own GitHub account.

2. Clone the repository to your local machine
```
$ git clone "https://gitlab.com/minor-1-swarm-intelligence/minor-1-swarm-intelligence"
```
where username is your GitHub account username.

3. Create a branch where you can do your local work.
Never work on **master** branch as we do not allow master commits except by admins.
```
$ git branch {branchname}
$ git checkout branchname
```

4. Do your work and stage your changes.
```
$ git add <filename>
```

5. Commit you changes with a commit message containing your name, file(s) worked upon, changes added.
```
$ git commit -m "Name| files| Changes"
```

6. Push changes to your forked repository
```
$ git push -u origin branchname
```

# Synchronize forked repository with Upstream repository

1. Create upstream as our repository
```
$ git remote add upstream "https://gitlab.com/minor-1-swarm-intelligence/minor-1-swarm-intelligence"
```

2. Fetch upstream changes in local machine
```
$ git fetch upstream
```

3. Switch to master branch
```
$ git checkout master
```

4. Merge changes in local machine
```
$ git merge upstream/master
```

5. Push changes to your forked GitHub repository
```
$ git push -f origin master
```

## Pert Chart

![Pertchart](img/Pert_chart.png)

# License: 

Authorized [MIT](LICENSE) License 
